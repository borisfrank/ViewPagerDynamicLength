package com.mbsystemes.dynamicviewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> categories = new ArrayList<>();

    @BindView(R.id.viewpager)
    ViewPager viewpager;

    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        categories.add("Page 1");
        categories.add("Page 2");
        categories.add("Page 3");
        categories.add("Page 4");
        categories.add("Page 5");
        categories.add("Page 6");

        setTitle(categories.get(0));

        initViewPager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            categories.add("Page added");
            pagerAdapter.addFrag(BlankFragment.newInstance("Page added"), "no");
            pagerAdapter.notifyDataSetChanged();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViewPager() {
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        for(String page: categories) {
            pagerAdapter.addFrag(BlankFragment.newInstance(page), "no");
        }
        viewpager.setAdapter(pagerAdapter);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setTitle(categories.get(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
